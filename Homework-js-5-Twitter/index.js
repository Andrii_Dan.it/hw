const urlUsers = 'https://ajax.test-danit.com/api/json/users';
const urlPosts = 'https://ajax.test-danit.com/api/json/posts';
const root = document.querySelector('#root');


class Card {
    constructor(urlUsers, root, urlPosts) {
        this.urlUsers = urlUsers;
        this.root = root;
        this.urlPosts = urlPosts;
        this.formFragment = document.querySelector("#posts-list").content;
        this.list = this.formFragment.querySelector("div");
    }


    request(url) {
        return fetch(`${url}`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response)
            } else {
                return response.json();
            }
        }).catch((e) => console.log(e));
    }



    renderPosts() {
        this.request(this.urlUsers).then((usersArr) => {

            this.request(this.urlPosts).then((postsArr) => {
                postsArr.map(({ title, body, id, userId }) => {
                    const divUsers = document.createElement("div");
                    divUsers.classList.add("twitter-tweet");
                    divUsers.innerHTML =
                        `<p>title: ${title}</p>
                    <p>text: ${body}</p>`
                    const delBtn = document.createElement("button");
                    delBtn.type = "button";
                    delBtn.textContent = "x";
                    divUsers.append(delBtn);
                    this.list.append(divUsers)

                    usersArr.forEach(usersObj => {
                        if (usersObj.id === userId) {
                            divUsers.insertAdjacentHTML('afterbegin',
                                `<p>email: ${usersObj.email}</p>
                                    <p>name: ${usersObj.name}</p>`
                            );
                        }
                    });
                    delBtn.addEventListener('click', () => {
                        console.log(userId);
                        this.removeElement(this.urlPosts, userId, divUsers)
                    })

                })
            })
        })
        this.root.append(this.list);

    }

    removeElement(urlDel, idDel, divUsers) {
        return fetch(`${urlDel}/${idDel}`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response)
            } else {
                divUsers.remove();
            }
        }).catch((e) => console.log(e));
    }

}
const card = new Card(urlUsers, root, urlPosts);
card.renderPosts();
