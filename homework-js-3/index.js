const container = document.createElement("div");
container.id = "root";
const list = document.createElement("ul");
container.append(list);
document.body.appendChild(container);   


const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
   author: "Террі Пратчетт",
   name: "Рухомі картинки",
   price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

  books.forEach(books => {
    try {
      if ('price' in books == false ){
        throw new Error('we have not price!');
      }else if ('author' in books == false ){
        throw new Error('we have not author!');
      }else if ('name' in books == false ){
        throw new Error('we have not name!');
      }else {
        let item = document.createElement("li");
        list.append(item);
        for (let key in books) {
                let pItem = document.createElement("p");
                  item.append(pItem);
                  pItem.append(` ${key}: ${books[key] + ";\n\n"} `);
        }
      }
    }catch (e) {
        console.log(e.name + ': ' + e.message);
    }
  });