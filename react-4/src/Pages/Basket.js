import React, { useState } from 'react'
import { Link, useLocation } from "react-router-dom";
import BasketItem from '../components/BasketItem';
import { AiOutlineStar } from 'react-icons/ai';
import { FaShoppingCart } from 'react-icons/fa';
import { useDispatch, useSelector } from 'react-redux';
import Modal from '../components/Modal';
import { closeModalReduser } from '../store/actionCreater';

export default function Basket({  changeCount, deleteOrder }) {
    const modal = useSelector(state => state.modal);
    const modalBoolean = modal.modal;
    const dispatch = useDispatch()
    const closeModal = () => {
        return dispatch(closeModalReduser())
    }
    const buyItem = () => {
        return dispatch(closeModalReduser())
    }
    const ordersId = JSON.parse(localStorage.getItem('ordersId'));
    const selector = useSelector(state => state.items.items.filter(card => ordersId.includes(card.id)));

    return (
        <div >
            <div className='header header__inner'>
                <Link to="/selected"><AiOutlineStar className='aiOutlineStarHeader' /></Link>

                <Link className='Link' to="/" >Home</Link>
                <Link to={"/basket"} > <FaShoppingCart className='FaShoppingCart__active FaShoppingCart' /></Link>

            </div>
            <div className='basket__items'>
                {
                    selector && selector.map(({ name, url, price, id, color }) => (
                        <BasketItem changeCount={changeCount} deleteOrder={deleteOrder} id={id} key={id} name={name} url={url} price={price} color={color}></BasketItem>
                    ))
                }
                {modalBoolean &&
                    <Modal
                        closeModal={closeModal}
                        deleteOrder={deleteOrder}
                        buyItem={buyItem}>
                    </Modal>
                }
            </div>

        </div >

    )
}

