import { USERS_SEND } from "./actionType"

const defaultState = {
    items: []
}

export const itemsReducer = (state = defaultState, {type,payload}) => {
    switch (type) {
        case USERS_SEND:
            return {
                ...state,
                items: [...state.items, ...payload]
            }
        default: return state
    }
}

