import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import { itemsReducer } from "./itemsReduser";
import thunk from "redux-thunk";
import { modalReduser } from "./modalReduser";

const rootReducer = combineReducers({
    items: itemsReducer,
    modal: modalReduser,
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
