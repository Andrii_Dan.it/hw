import { CLOSE_MODAL, USERS_SEND,BUY_ITEM, DELETE_ORDER,ADD_ORDER  } from "./actionType";

export const addUserAction = (payload) => ({ type: USERS_SEND, payload }) 
export const addOrderReduser = (payload) => ({ type: ADD_ORDER, payload })
export const deleteOrderReduser = (payload) => ({ type: DELETE_ORDER, payload })
export const addItemReduser = (payload) => ({ type: BUY_ITEM, payload })
export const closeModalReduser = () => ({ type: CLOSE_MODAL })