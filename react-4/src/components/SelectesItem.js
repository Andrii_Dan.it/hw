import { AiOutlineStar } from 'react-icons/ai';
import React, { useState, useEffect } from 'react'

export default function SelectesItem({ name, url, changeCount, deleteSelected, price, favouritId, id, color }) {
    const [star, setstar] = useState(false)
    const [modal, setModal] = useState(false)

    const openEnter = () => {
        return setModal(!modal)
    };

    useEffect(() => {
        if (favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
    }, [])
    const starCount = () => {
        if (!favouritId.includes(id)) {
            setstar(true)

        } else {
            setstar(false)


        }
        changeCount(id)

    }
    return (
        <div>
            <div className="card card__basket">
                <div className="closeModal" onClick={() => { deleteSelected(id); }}></div>

                <img className='card__photo' src={url} alt="Denim Jeans" ></img>

                <div className="card__details">
                    <span className="tag">{price} UAN</span>
                    <div className="name">{name}</div>
                    <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={starCount} className='aiOutlineStarItem' />
                    <button className='buttonCard' onClick={openEnter}>Buy</button>
                </div>
            </div>
        </div >
    )
}
