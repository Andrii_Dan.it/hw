import React from 'react'
import Item from './Item'
import { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

export default function Items({ favouritId, addOrder, counter }) {

    const selector = useSelector(state => state.items.items);

    return (
        <div>
            {
                selector.map(({ name, url, price, id, color, typeofItem }) => (
                    < Item favouritId={favouritId} addOrder={addOrder} shop={selector} counter={counter} typeofItem={typeofItem} id={id} key={id} name={name} price={price} url={url} color={color} ></Item>
                ))
            }
        </div >
    )
}

Items.propTypes = {
    addOrder: PropTypes.func.isRequired,
    counter: PropTypes.func.isRequired,
}
