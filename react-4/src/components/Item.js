import React from 'react'
import '../scss/item.scss'
import { AiOutlineStar } from 'react-icons/ai';
import { useState, useEffect } from "react";
import Modal from './Modal';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { addOrderReduser, closeModalReduser } from '../store/actionCreater';

export default function Item({ favouritId, addOrder, counter, name, url, price, typeofItem, id, }) {
    const [star, setstar] = useState(false)
    const modalBoolean = useSelector(state => state.modal.modal)
    const dispatch = useDispatch();

    useEffect(() => {
        if (favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
    }, [])
    const starCount = () => {
        if (!favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
        counter(id)
    }
    const openEnter = () => {
        return dispatch(addOrderReduser(id))
    };
    const closeModal = () => {
        return dispatch(closeModalReduser())
    }


    return (
        <div>
            <div id="container">
                <div className="card">
                    <img className='card__photo' src={url} alt="Denim Jeans" ></img>

                    <div className="card__details">
                        <span className="tag">{price} UAN</span>
                        <div className="name">{name}</div>
                        <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={starCount} className='aiOutlineStarItem' />
                        <button className='buttonCard' onClick={openEnter}>Add to card</button>
                        {modalBoolean == id &&
                            <Modal
                                closeModal={closeModal}

                                addOrder={addOrder}>
                            </Modal>
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

Item.propTypes = {
    addOrder: PropTypes.func.isRequired,
    counter: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    typeofItem: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,

}