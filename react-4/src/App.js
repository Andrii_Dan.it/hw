import React, { useState, useEffect } from 'react'
import { Routes, Route } from "react-router-dom";
import Basket from './Pages/Basket';
import NoPage from './Pages/NoPage';
import Selected from './Pages/Selected';
import Home from './Pages/Home';
import { useDispatch } from 'react-redux';
import { fetchUsers } from './asyncActions/fetchUsers';

export default function App() {
  const [ordersId, setOrdersId] = useState([])
  const [favouritId, setFavouritId] = useState([])
  const dispatch = useDispatch();

  const addOrder = (id) => {
    if (!ordersId.includes(id)) {
      setOrdersId((prev) => {
        const newArr = [id, ...prev]
        localStorage.setItem("ordersId", JSON.stringify(newArr));
        return newArr;
      })
    }
  }
  const deleteOrder = (id) => {
    setOrdersId((prev) => {
      const newArr = prev.filter((ordersId) => (ordersId !== id))

      localStorage.setItem("ordersId", JSON.stringify(newArr));
      return newArr;
    })
  }

  useEffect(() => {
    dispatch(fetchUsers());
    const orderArr = JSON.parse(localStorage.getItem('ordersId'));
    if (orderArr) {
      setOrdersId(orderArr);
    }
    const starArr = JSON.parse(localStorage.getItem('favouritId'));
    if (starArr) {
      setFavouritId(starArr);
    }

  }, [])

  const changeCount = (id) => {

    if (!favouritId.includes(id)) {
      setFavouritId((prev) => {
        const newArr = [id, ...prev]
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })
    } else {
      setFavouritId((prev) => {
        const newArr = prev.filter((favouritId) => (favouritId !== id))
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })

    }

  }
  const deleteSelected = (id) => {
    if (favouritId.includes(id)) {
      setFavouritId((prev) => {
        const newArr = prev.filter((favouritId) => (favouritId !== id))
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })
    }
  }



  return (
    <div className="App">
      <>
        <Routes>

          <Route path="/" element={<Home
            orders={ordersId}
            addOrder={addOrder}
            counter={changeCount}
            favouritId={favouritId}
          />} />
          <Route path="/selected" element={<Selected
            deleteSelected={deleteSelected} changeCount={changeCount} 
          />} />
          <Route path="/basket" element={<Basket favouritId={favouritId} changeCount={changeCount} deleteOrder={deleteOrder}  />} />
          <Route path="*" element={<NoPage />} />
        </Routes>
      </>
    </div>
  );
}













