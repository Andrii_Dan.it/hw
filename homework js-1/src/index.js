class Employee {
    constructor (name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }

    get setName() {
        return this.name;
    }
    get setAge() {
        return this.age;
    }
    get setSalary() {
        return this.salary;
    }


    set setName(newName) {
        this.name = newName;
    }
    set setAge(newAge) {
        this.age = newAge;
    }
    set setSalary(newSalary) {
        this.salary = newSalary;
    }

}

class Programmer extends Employee {
    constructor (name, age, salary,lang) {
        super (name, age, salary)
        this.lang = lang;
    }

    get setSalary() {
        return this.salary * 3;
    }
}



const program = new Programmer ('Niky', '20', 30000, 'UA, EN');
console.log(program);


const programerJohn = new Programmer ('John', '30', 40000, 'UA');
console.log(programerJohn);



