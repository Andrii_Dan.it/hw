import gulp from 'gulp';
import concat from 'gulp-concat';
import autoprefixer from 'gulp-autoprefixer';
import cleanCSS from 'gulp-clean-css';
import uglify from 'gulp-uglify';
import gulpSass from 'gulp-sass';
import imagemin from 'gulp-imagemin';
import dartSass from 'sass';
import browserSync from 'browser-sync';
import imgCompress from 'imagemin-jpeg-recompress';
import { deleteAsync } from 'del';
const sass = gulpSass( dartSass );



const jsFile = [
    './src/js/burger-menu.js',
];
const cssFile = [
    './src/scss/index.scss',
];

function scripts() {
    return gulp.src(jsFile)
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream())
        .pipe(uglify({
            toplevel:true,
        }));
};

function styles() {
    return gulp.src(cssFile)
        .pipe(sass().on("error", sass.logError))
        .pipe(concat('styles.min.css'))
        .pipe(autoprefixer({
            overrideBrowserslist:['> 0.1%'],
			cascade: false
        }))
        .pipe(cleanCSS({level: 2}))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function clean() {
    return deleteAsync(['dist/**'])
};

function watch() {
    browserSync.init({
        server: {
            baseDir:"./"
        },
        browser: "google chrome",
        open: true,
    })
    gulp.watch('./src/js/**/*.js', scripts);
    gulp.watch("*.html").on("change", browserSync.reload);
    gulp.watch('./src/scss/**/*.scss', styles);
};

function img () {
    return gulp.src('./src/img/**/*')
    .pipe(imagemin([
      imgCompress({
        loops: 4,
        min: 70,
        max: 80,
        quality: 'high'
      }),
      imagemin.gifsicle(),
      imagemin.optipng(),
      imagemin.svgo()
    ]))
    .pipe(gulp.dest('dist/images'));
};

// gulp.task('img', function() {
//     return gulp.src('./src/img/**/*')
//     .pipe(imagemin([
//       imgCompress({
//         loops: 4,
//         min: 70,
//         max: 80,
//         quality: 'high'
//       }),
//       imagemin.gifsicle(),
//       imagemin.optipng(),
//       imagemin.svgo()
//     ]))
//     .pipe(gulp.dest('dist/images'));
//   });


gulp.task('build', gulp.series(clean,
    gulp.parallel( scripts, img, styles)));
    
gulp.task('dev', gulp.parallel(watch));

