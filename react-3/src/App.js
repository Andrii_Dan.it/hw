import React, { useState, useEffect } from 'react'
import { Routes, Route } from "react-router-dom";
import Basket from './Pages/Basket';
import NoPage from './Pages/NoPage';
import Selected from './Pages/Selected';
import Home from './Pages/Home';

export default function App() {
  const [shop, setShop] = useState([])
  const [ordersId, setOrdersId] = useState([])
  const [favouritId, setFavouritId] = useState([])

  const addOrder = (id) => {
    if (!ordersId.includes(id)) {
      setOrdersId((prev) => {
        const newArr = [id, ...prev]
        localStorage.setItem("ordersId", JSON.stringify(newArr));
        return newArr;
      })
    }
  }
  const deleteOrder = (id) => {
    console.log(id);
    setOrdersId((prev) => {
      const newArr = prev.filter((ordersId) => (ordersId !== id))
      localStorage.setItem("ordersId", JSON.stringify(newArr));
      return newArr;
    })
  }

  useEffect(() => {
    fetch("http://localhost:8000/shop")
      .then(res => res.json())
      .then(
        (result) => {
          setShop(result)
        },
        (error) => {
          console.log(error);
        }
      )
    const saved = +localStorage.getItem("count")
    if (!saved ) {
      localStorage.setItem("count", 0);
    }
    const savedItem = +localStorage.getItem("item")
    if (!savedItem) {
      localStorage.setItem("item", 0);
    }

  }, [])


  const changeCount = (id) => {

    if (!favouritId.includes(id)) {
      setFavouritId((prev) => {
        const newArr = [id, ...prev]
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })
    } else {
      setFavouritId((prev) => {
        const newArr = prev.filter((favouritId) => (favouritId !== id))
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })

    }

  }
  const deleteSelected = (id) => {
    if (favouritId.includes(id)) {
      setFavouritId((prev) => {
        const newArr = prev.filter((favouritId) => (favouritId !== id))
        localStorage.setItem("favouritId", JSON.stringify(newArr));
        return newArr;
      })
    }
  }
  return (
    <div className="App">
      <Routes>
        <Route path="/" element={<Home
          orders={ordersId}
          addOrder={addOrder}
          counter={changeCount}
          shop={shop}
          favouritId={favouritId}
        />} />
        <Route path="/selected" element={<Selected
          deleteSelected={deleteSelected} changeCount={changeCount} favouritId={favouritId} favourite={shop.filter(card => favouritId.includes(card.id))}
        />} />
        <Route path="/basket" element={<Basket favouritId={favouritId} changeCount={changeCount} deleteOrder={deleteOrder} order={shop.filter(card => ordersId.includes(card.id))} />} />
        <Route path="*" element={<NoPage />} />
      </Routes>
    </div>
  );
}











