import React from 'react'
import { Link, } from "react-router-dom";
import BasketItem from '../components/BasketItem';
import { AiOutlineStar } from 'react-icons/ai';
import { FaShoppingCart } from 'react-icons/fa';

export default function Basket({ favouritId, changeCount, order, deleteOrder }) {

    return (
        <div >
            <div className='header header__inner'>
                <Link to="/selected"><AiOutlineStar className='aiOutlineStarHeader' /></Link>

                <Link className='Link' to="/" >Home</Link>
                <Link to={"/basket"} > <FaShoppingCart className='FaShoppingCart__active FaShoppingCart' /></Link>

            </div>
            {
                order && order.map(({ name, url, price, id, color }) => (
                    <BasketItem favouritId={favouritId} changeCount={changeCount} deleteOrder={deleteOrder} id={id} key={id} name={name} url={url} price={price} color={color}></BasketItem>
                ))
            }

        </div>

    )
}

