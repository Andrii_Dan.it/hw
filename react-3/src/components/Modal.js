import React from 'react'
import '../scss/modal.scss'
import PropTypes from 'prop-types';

export default function Modal({ buyItem, deleteOrder, title, text, closeModal,addOrder, id }) {

    return (
        <div>
            <div className='modal-backdrop' onClick={closeModal} ></div>
            <div className='modal-content'>
                <header>
                    <h1>{title}</h1>
                </header>
                <span className='close' onClick={closeModal} >&times;</span>
                <p>{text}</p>
                {deleteOrder &&
                    <button onClick={() => {
                        closeModal();
                        deleteOrder(id);
                    }}>delete</button>

                }
                {addOrder &&
                    <button onClick={() => {
                        closeModal();
                        addOrder(id);
                    }}>ADD</button>
                }
                {buyItem &&
                    <button onClick={() => {
                        closeModal();
                        buyItem();
                    }}>BUY</button>
                }

            </div>
        </div>

    )
}

Modal.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    typeofItem: PropTypes.string,
    addOrder: PropTypes.func,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,

}
Modal.defaultProps = {
    title: 'Кошик',
    text: 'Підтвердити',
    typeofItem: 'true',
}