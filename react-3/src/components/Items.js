import React from 'react'
import Item from './Item'
import PropTypes from 'prop-types';

export default function Items({ favouritId, addOrder, shop, counter }) {


    return (
        <div>
            {
                shop.map(({ name, url, price, id, color, typeofItem }) => (
                    < Item favouritId={favouritId} addOrder={addOrder} shop={shop} counter={counter} typeofItem={typeofItem} id={id} key={id} name={name} price={price} url={url} color={color} ></Item>
                ))
            }
        </div >
    )
}

Items.propTypes = {
    addOrder: PropTypes.func.isRequired,
    shop: PropTypes.array.isRequired,
    counter: PropTypes.func.isRequired,
}
