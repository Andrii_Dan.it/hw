import { AiOutlineStar } from 'react-icons/ai';
import React, { useState, useEffect } from 'react'
import Modal from './Modal';

export default function BasketItem({ favouritId, changeCount, deleteOrder, id, name, url, price, color }) {
    const [star, setstar] = useState(false)
    const [modal, setModal] = useState(false)
    const [modalDelete, setModalDelete] = useState(false)

    const openEnter = () => {
        return setModal(!modal)
    };

    const openDelete = () => {
        return setModalDelete(!modalDelete)
    };

    const closeModal = () => {
        return setModal(false)
    }

    const buyItem = () => {
        return setModal(false)
    }

    useEffect(() => {
        if (favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
    }, [])
    const starCount = () => {
        if (!favouritId.includes(id)) {
            setstar(true)

        } else {
            setstar(false)
        }
        changeCount(id)

    }

    const titleOne = 'Видалення з кошика';
    const oneText = 'Підтвердіть для видалення товару з кошику';

    const titleBuy = 'Видалення з кошика';
    const buyText = 'Підтвердіть для видалення товару з кошику';
    return (
        <div>
            <div className="card card__basket">
                <div className="closeModal" onClick={() => { openDelete(); }} ></div>
                {modalDelete &&
                    <Modal
                        closeModal={closeModal}
                        text={oneText}
                        title={titleOne}
                        id={id}
                        deleteOrder={deleteOrder}
                        key={id}
                        name={name}
                        price={price}
                        url={url}
                        color={color}>
                    </Modal>
                }
                <img className='card__photo' src={url} alt="Denim Jeans" ></img>

                <div className="card__details">
                    <span className="tag">{price} UAN</span>
                    <div className="name">{name}</div>
                    <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={starCount} className='aiOutlineStarItem' />
                    <button className='buttonCard' onClick={openEnter}>Buy</button>
                    {modal &&
                        <Modal
                            closeModal={closeModal}
                            text={buyText}
                            title={titleBuy}
                            id={id}
                            buyItem={buyItem}
                            key={id}
                            name={name}
                            price={price}
                            url={url}
                            color={color}>
                        </Modal>
                    }
                </div>
            </div>
        </div>
    )
}
