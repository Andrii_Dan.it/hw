import React, { Component } from 'react'
import './scss/heder.scss'
import { FaShoppingCart } from 'react-icons/fa';
import { AiOutlineStar } from 'react-icons/ai';
import PropTypes from 'prop-types';

export default class Header extends Component {
    render() {
        const { lengthItem, count } = this.props;
        console.log('lengthItem' ,lengthItem, 'count',count );
        return (
            <div>
                <header className='header'>
                    <AiOutlineStar className='aiOutlineStarHeader' />
                    <p className='counter' >{count ? count : 0}</p>
                    <p className='item__number' >{lengthItem ? lengthItem : 0}</p>                  
                    <FaShoppingCart className='FaShoppingCart' />
                </header>
            </div>
        )
    }
}

Header.propTypes = {
    lengthItem: PropTypes.number.isRequired,
    count: PropTypes.number.isRequired,
}