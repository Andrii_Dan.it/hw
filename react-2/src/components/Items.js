import React, { Component } from 'react'
import Item from './Item'
import PropTypes from 'prop-types';

export default class Items extends Component {
    render() {
        const { addOrder, shop, counter } = this.props;
        return (
            <div>
                {
                    shop.map(({ name, url, price, id, color, typeofItem }) => (
                        < Item addOrder={addOrder} shop={shop} counter={counter} typeofItem={typeofItem} id={id} key={id} name={name} price={price} url={url} color={color} ></Item>
                    ))
                }
            </div >
        )
    }
}


Items.propTypes = {
    addOrder: PropTypes.func.isRequired,
    shop: PropTypes.array.isRequired,
    counter: PropTypes.func.isRequired,
}
