import React, { Component } from 'react'
import '../scss/modal.scss'
import PropTypes from 'prop-types';


export default class Modal extends Component {
    render() {
        const { title, text, closeModal, addOrder, id } = this.props
        return (
            <>
                <div className='modal-backdrop' onClick={closeModal} ></div>
                <div className='modal-content'>
                    <header>
                        <h1>{title}</h1>
                    </header>
                    <span className='close' onClick={closeModal} >&times;</span>
                    <p>{text}</p>
                    {addOrder &&
                    <button onClick={() => {
                        closeModal();
                        addOrder(id);
                    }}>ADD</button>
                }
                </div>
            </>
        )
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    addOrder: PropTypes.func.isRequired,
    id: PropTypes.number.isRequired,

}
Modal.defaultProps = {
    title: 'Кошик',
    text: 'Підтвердити',
    typeofItem: 'true',
}