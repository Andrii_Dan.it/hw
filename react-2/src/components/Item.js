import React, { Component } from 'react'
import '../scss/item.scss'
import { AiOutlineStar } from 'react-icons/ai';
import Modal from './Modal';
import PropTypes from 'prop-types';


export default class Item extends Component {
    state = {
        modal: false,
        star: false,
    }

    starCount = () => {
        this.setState({ star: !this.state.star })
        this.props.counter(!this.state.star)

    }
    openEnter = () => {
        this.setState({ modal: !this.state.modal })
    };
    closeModal = () => {
        this.setState({ modal: false })
    }

    render() {
        const { addOrder, color, name, url, price, typeofItem, id, } = this.props
        const { modal, star } = this.state
        const titleOne = 'Кошик';
        const oneText = 'Підтвердіть для перенесення товару в кошик';
        return (
            <div>
                <div id="container">
                    <div className="card">
                        <img className='card__photo' src={url} alt="Denim Jeans" ></img>

                        <div className="card__details">
                            <span className="tag">{price} UAN</span>
                            <div className="name">{name}</div>
                            <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={this.starCount} className='aiOutlineStarItem' />
                            <button className='buttonCard' onClick={this.openEnter}>Add to card</button>
                            {modal &&
                                <Modal
                                    closeModal={this.closeModal}
                                    text={oneText}
                                    title={titleOne}
                                    id={id}
                                    addOrder={addOrder}
                                    key={id}
                                    name={name}
                                    price={price}
                                    typeofItem={typeofItem}
                                    url={url}
                                    color={color}>
                                </Modal>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


Item.propTypes = {
    addOrder: PropTypes.func.isRequired,
    counter: PropTypes.func.isRequired,
    color: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    url: PropTypes.string.isRequired,
    price: PropTypes.string.isRequired,
    typeofItem: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,

}