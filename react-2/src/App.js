import React, { Component } from 'react'
import Items from './components/Items';
import Header from './Header';

export default class App extends Component {
  state = {
    modal: false,
    modalRegister: false,
    shop: [],
    isLoaded: false,
    count: +localStorage.getItem("count"),
    star: false,
    orders: [],
    lengthItem: +localStorage.getItem("item"),
  }

  addOrder = (...item) => {
    for (let i = this.state.orders.length - 1; i >= 0; i--) {
      if (this.state.orders[i].id === item[0].id) {
        this.state.orders.splice(i, 1);
      }
    }
    this.setState({ orders: [...this.state.orders, ...item] })
    this.setState({ lengthItem: this.state.orders.length + 1 })
    localStorage.setItem("item", this.state.orders.length + 1);
  }
  componentDidMount = () => {
    const saved = +localStorage.getItem("count")
    if (!saved) {
      localStorage.setItem("count", 0);
    }
    const savedItem = +localStorage.getItem("item")
    if (!savedItem) {
      localStorage.setItem("item", 0);
    }
    fetch("http://localhost:8000/shop")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            shop: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }


  closeModal = () => {
    this.setState({ modal: false, modalRegister: false })
  }
  changeCount = (starItem) => {
    if (starItem) {
      this.setState({ count: +this.state.count + 1 })
      localStorage.setItem("count", +this.state.count + 1);
    } else {
      this.setState({ count: +this.state.count - 1 })
      localStorage.setItem("count", +this.state.count - 1);
    }

  }


  render() {
    const { shop, count, star, lengthItem } = this.state;
    return (
      <div className="App">
        <>
          <Header lengthItem={lengthItem} count={count} />
          <Items addOrder={this.addOrder} star={star} counter={this.changeCount} shop={shop} ></Items>

        </>
      </div>
    );
  }
}














// import React, { Component } from 'react'
// import Items from './components/Items';
// import Modal from './components/Modal';
// import Header from './Header';

// export default class App extends Component {
//   state = {
//     modal: false,
//     modalRegister: false,
//     shop: [],
//     isLoaded: false,
//     count: localStorage.getItem("count"),
//     item: localStorage.getItem("item"),
//     star:false
//   }

//   // this.setState({ star: !this.state.star })
//   //   console.log(this.state.star);
//   //   if (this.state.star == false) {
//   //     console.log('+1');
//   //     this.setState({ count: +this.state.count + 1 })
//   //     localStorage.setItem("count", +this.state.count + 1);
//   //   } else {
//   //     this.setState({ count: this.state.count - 1 })
//   //     localStorage.setItem("count", this.state.count - 1);
//   //   }

//   componentDidMount = () => {
//     const saved = localStorage.getItem("count")
//     if (saved == undefined) {
//       localStorage.setItem("count", 0);
//     }
//     const savedItem = localStorage.getItem("item")
//     if (savedItem == undefined) {
//       localStorage.setItem("item", 0);
//     }
//     fetch("http://localhost:3000/shop")
//       .then(res => res.json())
//       .then(
//         (result) => {
//           this.setState({
//             isLoaded: true,
//             shop: result,
//           });
//         },
//         (error) => {
//           this.setState({
//             isLoaded: true,
//             error
//           });
//         }
//       )
//   }
//   openEnter = () => {
//     this.setState({ modal: !this.state.modal }, () => {
//       console.log(this.state.modal);
//     });
//   };

//   closeModal = () => {
//     this.setState({ modal: false, modalRegister: false })
//   }

//   changeCount = () => {
//     this.setState({ count: +this.state.count + 1 })
//     console.log(this.state.count);

//     localStorage.setItem("count", +this.state.count + 1);
//   }
//   addItem = () => {
//     this.setState({ item: +this.state.item + 1 })
//     localStorage.setItem("item", +this.state.item + 1);

//   }
//   render() {
//     const { modal, shop, count, item } = this.state;
//     const titleOne = 'Кошик';
//     const oneText = 'Підтвердіть для перенесення товару в кошик';
//     return (
//       <div className="App">
//         <>
//           <Header item={item} count={count} />
//           {modal &&
//             <Modal text={oneText} title={titleOne} addItem={this.addItem} closeModal={this.closeModal}></Modal>
//           }
//           <Items changeCount={this.changeCount} onClick={this.openEnter} shop={shop} ></Items>

//         </>
//       </div>
//     );
//   }
// }

