import './modal.scss'
import React, { Component } from 'react'

export default class Modal extends Component {
    render() {
        const { header, actions, closeButton, text } = this.props
        return (
            <>
                <div className='modal-backdrop' onClick={closeButton} ></div>
                <div className='modal-content'>
                    <header>
                        <h1>{header}</h1>
                    </header>
                    <span className='close' onClick={closeButton} >&times;</span>
                    <p>{text}</p>
                    {actions}
                </div>

            </>
        )
    }
}
