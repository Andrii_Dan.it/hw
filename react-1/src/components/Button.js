import React, { Component } from 'react'
import './button.scss'



export default class Button extends Component {

    render() {
        const { backgroundColor, text, onClick } = this.props;
        return (
            <div>
                <button className='button' style={{ background: backgroundColor }} onClick={onClick}>{text}</button>
            </div>
        )
    }
}

