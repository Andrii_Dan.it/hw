import React, { Component } from 'react'
import Button from './components/Button';
import Modal from './components/Modal';

export default class App extends Component {
  state = {
    titleOne: 'Enter',
    titleTwo: 'Register',
    modalBool: false,
    closeModal: false,
    titleModal: '',
    actions: ''
  }


  openEnter = () => {
    this.setState({
      closeModal: !this.state.closeModal,
      textModal: "Open first modal",
      titleModal: "Enter",
      actions:<div className='modal__buttons'>
        <button className='modal__button'>Enter</button>
      <button className='modal__button' onClick={this.closeButton}>Remove</button>
      </div>
    })
  };

  openRegister = () => {
    this.setState({
      closeModal: !this.state.closeModal,
      textModal: "Open second modal",
      titleModal: "Register",
      actions:<div className='modal__buttons'>
        <button className='modal__button'>Register</button>
      <button className='modal__button' onClick={this.closeButton}>Remove</button>
      </div>
    })
  }

  closeButton = () => {
    this.setState({ closeModal: false })
  }

  render() {
    const { closeModal, textModal, actions, titleModal } = this.state;
    const colorRegister = '#d0ff009e';
    const colorEnter = '#0092ff'
    const enter = "Enter"
    const register = "Register"
    return (
      <div className="App">
          <Button backgroundColor={colorEnter} onClick={this.openEnter} text={enter} />
          <Button backgroundColor={colorRegister} onClick={this.openRegister} text={register} />
          {closeModal &&
            <Modal text={textModal} header={titleModal} actions={actions} closeButton={this.closeButton} />
          }
      </div>
    );
  }
}








