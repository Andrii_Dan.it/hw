import { addUsersReducer } from "../store/itemsReduser"

export const fetchUsers = () => {
    return (dispatch) => {
        fetch("http://localhost:8000/shop")
            .then(res => res.json())
            .then(
                (result) => {
                    dispatch(addUsersReducer(result))
                }
            )
    }
}