const defaultState = {
    modal: null,
    header: "Кошик",
    textButton: '',
    text: "Підтвердіть для перенесення товару в кошик"
}
const BUY_ITEM = "BUY_ITEM"
const DELETE_ORDER = "DELETE_ORDER"
const ADD_ORDER = "ADD_ORDER"
const CLOSE_MODAL = "CLOSE_MODAL"

export const modalReduser = (state = defaultState, action) => {
    switch (action.type) {
        case BUY_ITEM:
            return {
                ...state,
                modal: action.payload,
                textButton: "Finish Buy",
                header: "Купівля",
                text: "Підтвердіть для завершення покупки",
            }
        case DELETE_ORDER:
            return {
                ...state,
                modal: action.payload,
                header: "Видалення з кошика",
                textButton: "DELETE",
                text: "Підтвердіть для видалення товару з кошику",
            }
        case ADD_ORDER:
            return {
                ...state,
                modal: action.payload,
                textButton: "BUY",
                header: "Кошик",
                text: "Підтвердіть для перенесення товару в кошик"
            }
        case CLOSE_MODAL:
            return {
                ...state,
                modal: null
            }
        default: return state
    }
}
export const addOrderReduser = (payload) => ({ type: ADD_ORDER, payload })
export const deleteOrderReduser = (payload) => ({ type: DELETE_ORDER, payload })
export const addItemReduser = (payload) => ({ type: BUY_ITEM, payload })
export const closeModalReduser = () => ({ type: CLOSE_MODAL })