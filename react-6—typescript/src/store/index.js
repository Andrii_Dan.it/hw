import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from 'redux-devtools-extension';
import { itemsReducer } from "./itemsReduser";
import thunk from "redux-thunk";
import { modalReduser } from "./modalReduser";
import { formReduser } from "./formReduser";

const rootReducer = combineReducers({
    basket: formReduser,
    items: itemsReducer,
    modal: modalReduser,
})

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))
