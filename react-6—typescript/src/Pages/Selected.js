import React from 'react'
import { Link } from "react-router-dom";
import SelectesItem from '../components/SelectesItem';
import { FaShoppingCart } from 'react-icons/fa';
import { AiOutlineStar } from 'react-icons/ai';

export default function Selected({ changeCount, favouritId, deleteSelected, favourite }) {
    return (
        <div>
            <div className='header header__inner'>
                <Link to="/selected"><AiOutlineStar className='aiOutlineStarHeader__active aiOutlineStarHeader' /></Link>

                <Link className='Link' to="/" >Home</Link>
                <Link to={"/basket"} > <FaShoppingCart className=' FaShoppingCart' /></Link>

            </div>
            {
                favourite && favourite.map(({ name, url, price, id, color }) => (
                    <SelectesItem changeCount={changeCount} favouritId={favouritId} deleteSelected={deleteSelected} key={id} id={id} name={name} url={url} price={price} color={color}></SelectesItem>
                ))
            }
        </div>
    )
}
