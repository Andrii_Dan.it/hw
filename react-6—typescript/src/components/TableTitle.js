import React from 'react'

export default function TableTitle() {
    return (
        <div className='table__title'>
            <p className='table__title-img'>IMAGE</p>
            <p className='table__title-price'>PRICE</p>
            <p className='table__title-name'>NAME</p>
            <p className='table__title-star'>STAR</p>
            <p className='table__title-basket'>BASKET</p>
        </div>
    )
}
