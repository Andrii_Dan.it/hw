import { AiOutlineStar } from 'react-icons/ai';
import React, { useState, useEffect } from 'react'
import Modal from './Modal';
import { useDispatch, useSelector } from 'react-redux';
import { addItemReduser, addOrderReduser, closeModalReduser, deleteOrderReduser } from '../store/modalReduser';

export default function BasketItem({ favouritId, changeCount, deleteOrder, id, name, url, price, color }) {
    const [star, setstar] = useState(false)
    const modal = useSelector(state => state.modal);
    const modalBoolean = modal.modal;
    const dispatch = useDispatch()

    const openEnter = () => {
        return dispatch(addItemReduser(id))
    };

    const openDelete = () => {
        return dispatch(deleteOrderReduser(id))
    };

    useEffect(() => {
        if (favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
    }, [])
    const starCount = () => {
        if (!favouritId.includes(id)) {
            setstar(true)

        } else {
            setstar(false)


        }
        changeCount(id)

    }

    const titleOne = 'Видалення з кошика';
    const oneText = 'Підтвердіть для видалення товару з кошику';

    const titleBuy = 'Видалення з кошика';
    const buyText = 'Підтвердіть для видалення товару з кошику';
    return (
        <div className="card card__basket">
            <div className="closeModal" onClick={() => { openDelete(); }} ></div>

            <img className='card__photo' src={url} alt="Denim Jeans" ></img>

            <div className="card__details">
                <span className="tag">{price} UAN</span>
                <div className="name">{name}</div>
                <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={starCount} className='aiOutlineStarItem' />
                <button className='buttonCard' onClick={openEnter}>Buy</button>
            </div>
        </div>

    )
}
