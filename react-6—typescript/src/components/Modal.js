import React from 'react'
import '../scss/modal.scss'
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

export default function Modal({ buyItem, deleteOrder, closeModal, addOrder }) {
    const modal = useSelector(state => state.modal);
    const modalHeader = modal.header
    const modalText = modal.text
    const modalId = modal.modal
    const modalTextButton = modal.textButton
    return (
        <>
            <div className='modal-backdrop' onClick={closeModal} ></div>
            <div className='modal-content'>
                <header>
                    <h1>{modalHeader}</h1>
                </header>
                <span className='close' onClick={closeModal} >&times;</span>
                <p>{modalText}</p>
                {modalHeader === 'Видалення з кошика' &&
                    <button onClick={() => {
                        closeModal();
                        deleteOrder(modalId);
                    }}>{modalTextButton}</button>
                }
                {addOrder &&
                    <button onClick={() => {
                        closeModal();
                        addOrder(modalId);
                    }}>{modalTextButton}</button>
                }
                {modalHeader === 'Купівля' &&
                    <button onClick={() => {
                        closeModal();
                        buyItem();
                    }}>{modalTextButton}</button>
                }

            </div>
        </>

    )
}

Modal.propTypes = {
    title: PropTypes.string,
    text: PropTypes.string,
    closeModal: PropTypes.func.isRequired,
    typeofItem: PropTypes.string,
    addOrder: PropTypes.func,
}
Modal.defaultProps = {
    title: 'Кошик',
    text: 'Підтвердити',
    typeofItem: 'true',
}