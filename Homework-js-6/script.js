const root = document.querySelector('#root');
const url = 'https://api.ipify.org/?format=json';
const urlIp = 'http://ip-api.com/json/';


class FindIp {
    constructor(url, urlIp) {
        this.template = document.querySelector('#ip-button').content;
        this.container = this.template.querySelector('.container');
        this.button = this.container.querySelector('button');
        this.url = url;
        this.urlIp = urlIp;
        this.addurl = '?fields=status,continent,country,countryCode,regionName,city';
    }
    render() {
        this.button.addEventListener('click', () => {

            this.request(this.url).then(({ ip }) => {
                this.request(this.urlIp, ip, this.addurl)
                    .then(({ country, city, regionName, continent }) => {
                        const infoForm = document.createElement('div');
                        infoForm.classList.add('info')
                        infoForm.innerHTML =
                            `<p>continent: ${continent}</p>
                            <p>country: ${country}</p>
                            <p>city: ${city}</p>
                            <p>regionName: ${regionName}</p>`
                        this.container.append(infoForm);

                    })
            })
        })
        root.append(this.container)
    }
    
    async request(url, ip = '', addurl = '') {
        return await fetch(`${url}${ip}${addurl}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
            },
        }).then((response) => {
            if (!response.ok) {
                throw new Error(response)
            } else {
                return response.json();
            }
        }).catch((e) => console.log(e));
    }

}

const findIp = new FindIp(url, urlIp);
findIp.render();



