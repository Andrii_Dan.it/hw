import { BUY_ITEM ,DELETE_ORDER,ADD_ORDER,CLOSE_MODAL} from "./actionType"

const defaultState = {
    modal: null,
    header: "Кошик",
    textButton: '',
    text: "Підтвердіть для перенесення товару в кошик"
}


export const modalReduser = (state = defaultState, {type,payload}) => {
    switch (type) {
        case BUY_ITEM:
            return {
                ...state,
                modal: payload,
                textButton: "Finish Buy",
                header: "Купівля",
                text: "Підтвердіть для завершення покупки",
            }
        case DELETE_ORDER:
            return {
                ...state,
                modal: payload,
                header: "Видалення з кошика",
                textButton: "DELETE",
                text: "Підтвердіть для видалення товару з кошику",
            }
        case ADD_ORDER:
            return {
                ...state,
                modal: payload,
                textButton: "BUY",
                header: "Кошик",
                text: "Підтвердіть для перенесення товару в кошик"
            }
        case CLOSE_MODAL:
            return {
                ...state,
                modal: null
            }
        default: return state
    }
}
