import React from 'react'
import Header from '../components/Header'
import Items from '../components/Items';

export default function Home({ favouritId, orders, counter, addOrder }) {




    return (
        <div>
            <Header favouritId={favouritId} orders={orders} />
            <Items favouritId={favouritId} addOrder={addOrder} counter={counter} ></Items>

        </div>

    )
}
