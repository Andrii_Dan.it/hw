import { Formik } from 'formik';
import React from 'react'
import '../scss/form.scss'
import * as yup from 'yup';


export default function Form({ order }) {
    const validationSchema = yup.object().shape({
        name: yup.string()
            .typeError('має бути строка')
            .required('Обовязково'),
        surname: yup.string().typeError('має бути строка').required('Обовязково'),
        age: yup.number().typeError('має бути число').required('Обовязково'),
        phone: yup.number().typeError('має бути число').required('Обовязково'),
        adress: yup.string().typeError('має бути число').required('Обовязково'),

    })

    return (
        <div>
            <Formik
                initialValues={{
                    name: '',
                    surname: '',
                    age: '',
                    adress: '',
                    phone: '',
                }}
                validateOnBlur
                onSubmit={(value) => {
                    console.log(value);
                    localStorage.removeItem('ordersId')
                    order.forEach(({ name, price, id }) => {
                        console.log("You bought:", "name:", name, ", price:", price, " id:", id);
                    });
                }}
                validationSchema={validationSchema}
            >
                {({ values, errors, touched, handleChange, handleBlur, isValid, handleSubmit, dirty }) => (
                    <div className='form' >
                        <p>
                            <label className='label__form' htmlFor={`name`} >Name</label><br />
                            <input
                                type="text"
                                name="name"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.name}
                                className='field-style field-split align-left'>
                            </input>
                        </p>
                        {touched.name && errors.name && <p className='error__input'>{errors.name}</p>}

                        <p>
                            <label className='label__form' htmlFor={`surname`} >surname</label><br />
                            <input
                                type="text"
                                name="surname"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.surname}
                                className='field-style field-split align-left'>
                            </input>
                        </p>
                        {touched.surname && errors.surname && <p className='error__input'>{errors.surname}</p>}

                        <p>
                            <label className='label__form' htmlFor={`age`} >age</label><br />
                            <input
                                type="text"
                                name="age"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.age}
                                className='field-style field-split align-left'>
                            </input>
                        </p>
                        {touched.age && errors.age && <p className='error__input'>{errors.age}</p>}


                        <p>
                            <label className='label__form' htmlFor={`adress`} >adress</label><br />
                            <input
                                type="text"
                                name="adress"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.adress}
                                className='field-style field-split align-left'>
                            </input>
                        </p>
                        {touched.adress && errors.adress && <p className='error__input'>{errors.adress}</p>}


                        <p>
                            <label className='label__form' htmlFor={`phone`} >phone</label><br />
                            <input
                                type="text"
                                name="phone"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.phone}
                                className='field-style field-split align-left'>
                            </input>
                        </p>
                        {touched.phone && errors.phone && <p className='error__input'>{errors.phone}</p>}


                        <button
                            className='buttonCard button__form'
                            type="submit"
                            onClick={handleSubmit}
                            disabled={!isValid && !dirty}>
                            Checkout
                        </button>
                    </div>
                )}
            </Formik>
        </div>
    )
}
