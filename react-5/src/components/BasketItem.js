import { AiOutlineStar } from 'react-icons/ai';
import React, { useState, useEffect } from 'react'
import { useDispatch } from 'react-redux';
import { addItemReduser, deleteOrderReduser } from '../store/actionCreater';

export default function BasketItem({  changeCount, id, name, url, price }) {
    const [star, setstar] = useState(false)
    const dispatch = useDispatch()
    const favouritId = JSON.parse(localStorage.getItem('favouritId'))

    const openEnter = () => {
        return dispatch(addItemReduser(id))
    };

    const openDelete = () => {
        return dispatch(deleteOrderReduser(id))
    };

    useEffect(() => {
        if (favouritId.includes(id)) {
            setstar(true)
        } else {
            setstar(false)
        }
    }, [])
    const starCount = () => {
        if (!favouritId.includes(id)) {
            setstar(true)

        } else {
            setstar(false)


        }
        changeCount(id)

    }


    return (
        <div className="card card__basket">
            <div className="closeModal" onClick={() => { openDelete(); }} ></div>

            <img className='card__photo' src={url} alt="Denim Jeans" ></img>

            <div className="card__details">
                <span className="tag">{price} UAN</span>
                <div className="name">{name}</div>
                <AiOutlineStar style={{ color: star ? 'red' : '' }} onClick={starCount} className='aiOutlineStarItem' />
                <button className='buttonCard' onClick={openEnter}>Buy</button>
            </div>
        </div>

    )
}
