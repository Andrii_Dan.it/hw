const url = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('#root');

class Films {
    constructor(url, root, entity) {
        this.url = url;
        this.root = root;
        this.entity = entity;
        this.formFragment = document.querySelector("#posts-list").content;
        this.list = this.formFragment.querySelector("ul");
    }
    request(characters = '') {
        return fetch(`${this.url}/${characters}`, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
            },
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error(response);
                } else {
                    return response.json();
                }
            })
            .catch((e) => {
                console.log(e);
            })
    }
    renderCharacters(characters) {
        return new Promise((resolve, reject) => {
            const xhr = new XMLHttpRequest();
            xhr.open("GET", `${characters}`);
            xhr.send();

            xhr.onload = () => resolve(xhr.response);
            xhr.onerror = () => reject(e);
        });
    }
    render() {
        this.request().then((response) => {
            response.map(({ episodeId, name, openingCrawl, characters }) => {
                const listItem = document.createElement("li");
                const episodeIdElement = document.createElement("p");
                const nameElement = document.createElement("ul");
                const openingCrawlElement = document.createElement("p");
                episodeIdElement.textContent = episodeId;
                nameElement.textContent = name;
                nameElement.style = 'font-weight: bold;';
                openingCrawlElement.textContent = openingCrawl;
                listItem.append(episodeIdElement);
                listItem.append(nameElement);
                listItem.append(openingCrawlElement);
                this.list.append(listItem);
                this.characters = this.episodeId;

                const charactersInfo = characters.map((e) => this.renderCharacters(e))

                Promise.allSettled(charactersInfo).then((data) => {
                    data.forEach(({ value }) => {
                        const name = JSON.parse(value).name;
                        const actors = document.createElement("li");
                        actors.textContent = name;
                        actors.style = 'font-weight: normal;'
                        nameElement.append(actors);
                    });
                });
            })
        })
        root.append(this.list);
    }

}

const films = new Films(url, root);
films.render();
