// const [star, setStar] = useState(false)
const defaultState = {
  items: [],
};
const USERS_SEND = "USERS_SEND";

export const itemsReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "USERS_SEND":
      return {
        ...state,
        items: [...state.items, ...action.payload],
      };
    default:
      return state;
  }
};

export const addUsersReducer = (payload) => ({ type: USERS_SEND, payload });
