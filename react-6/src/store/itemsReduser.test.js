import { addUsersReducer, itemsReducer } from "./itemsReduser";

describe("reducers function", () => {
  it("testing itemReducer", () => {
    const defaultState = {
      items: [],
    };
    const mockData = [
      {
        cardId: 1,
        name: "Аккумуляторна стрічкова пила ",
        price: "13073",
        vendorCode: "dpb182z",
        url: "./images/akkumulyatornaya-lentochnaya-pila-dpb182z-bez-akb.jpg",
        power: "350 Вт",
      },
      {
        cardId: 2,
        name: "Аккумуляторна стрічкова пила ",
        price: "13073",
        vendorCode: "dpb182z",
        url: "./images/akkumulyatornaya-lentochnaya-pila-dpb182z-bez-akb.jpg",
        power: "350 Вт",
      },
    ];
    const mockDataExpect = {
      items: [
        {
          cardId: 1,
          name: "Аккумуляторна стрічкова пила ",
          price: "13073",
          vendorCode: "dpb182z",
          url: "./images/akkumulyatornaya-lentochnaya-pila-dpb182z-bez-akb.jpg",
          power: "350 Вт",
        },
        {
          cardId: 2,
          name: "Аккумуляторна стрічкова пила ",
          price: "13073",
          vendorCode: "dpb182z",
          url: "./images/akkumulyatornaya-lentochnaya-pila-dpb182z-bez-akb.jpg",
          power: "350 Вт",
        },
      ],
    };

    expect(itemsReducer(defaultState, addUsersReducer(mockData))).toStrictEqual(
      mockDataExpect
    );
  });
});
