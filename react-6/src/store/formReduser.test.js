import { cleanBasletStorage, formReduser } from "./formReduser";

describe("reducers function", () => {
  it("testing formReduser", () => {
    const mockData = 0;

    const mockDataExpect = {
      storage: 0,
    };

    expect(formReduser({}, cleanBasletStorage(mockData))).toStrictEqual(
      mockDataExpect
    );
  });
});
