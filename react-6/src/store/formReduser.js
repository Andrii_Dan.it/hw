const defaultState = {
    storage: localStorage.getItem('ordersId')
}

const CLEAN_LOCALSTORAGE = 'CLEAN_LOCALSTORAGE'

export const formReduser = (state = defaultState, action) => {

    switch (action.type) {
        case CLEAN_LOCALSTORAGE:
            return {
                ...state,
                storage: +action.payload,
            }
        default: return state

    }
}

export const cleanBasletStorage = (payload) => ({ type: CLEAN_LOCALSTORAGE, payload })