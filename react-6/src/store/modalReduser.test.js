import { addOrderReduser, modalReduser } from "./modalReduser";

describe("reducers function", () => {
  it("testing modalReduser", () => {
    const mockData = true;
    const mockDataExpect = {
      modal: true,
      textButton: "BUY",
      header: "Кошик",
      text: "Підтвердіть для перенесення товару в кошик",
    };

    expect(modalReduser({}, addOrderReduser(mockData))).toStrictEqual(
      mockDataExpect
    );
  });
});
