import React from "react";

export default function Button({ text, closeModal, Click, modalId }) {
  return (
    <button
      role="button"
      onClick={() => {
        closeModal();
        Click(modalId);
      }}
    >
      {text}
    </button>
  );
}
