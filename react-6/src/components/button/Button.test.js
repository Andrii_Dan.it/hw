import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import Button from "./Button";

describe("Button", () => {
  it(" test button prop text ", () => {
    const { getByText } = render(<Button text="test" />);
    const buttonEl = getByText(/test/i);
    expect(buttonEl).toBeInTheDocument;
  });
  it("test button prop modalId", () => {
    const { getByRole } = render(<Button modalId="12383" />);
    const buttonEl = getByRole("button");
    expect(buttonEl).toBeInTheDocument;
  });
  it("Test click event", () => {
    const mockCallBack = jest.fn();
    const mockCallBacc = jest.fn();

    render(<Button Click={mockCallBacc} closeModal={mockCallBack} />);
    const button = screen.getByRole("button");
    fireEvent.click(button);

    expect(mockCallBack).toHaveBeenCalled();
    expect(mockCallBacc).toHaveBeenCalled();
  });
});
