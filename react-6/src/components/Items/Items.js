import React, { useState } from "react";
import Item from "../Item";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { ThemeContext } from "../../context/themeContext";
import { nanoid } from "nanoid";
import TableTitle from "../TableTitle";

export default function Items({ favouritId, addOrder, counter }) {
  const [theme, setTheme] = useState(true);
  const selector = useSelector((state) => state.items.items);

  const changeThemes = () => {
    setTheme(!theme);
  };
  let table;
  if (theme === false) {
    table = <TableTitle key={nanoid()} />;
  }
  return (
    <ThemeContext.Provider value={{ theme }}>
      <div>
        <div className="buttons_theme">
          <button className="button__theme" onClick={changeThemes}>
            change
          </button>
        </div>
        {table}
        {selector.map(({ name, url, price, id, color, typeofItem }) => (
          <Item
            favouritId={favouritId}
            addOrder={addOrder}
            shop={selector}
            counter={counter}
            typeofItem={typeofItem}
            id={id}
            key={id}
            name={name}
            price={price}
            url={url}
            color={color}
          ></Item>
        ))}
      </div>
    </ThemeContext.Provider>
  );
}

Items.propTypes = {
  addOrder: PropTypes.func.isRequired,
  counter: PropTypes.func.isRequired,
};
