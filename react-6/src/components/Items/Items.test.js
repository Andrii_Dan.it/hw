import * as reactRedux from "react-redux";
import { fireEvent, render, screen } from "@testing-library/react";
import { useSelector } from "react-redux";
import Items from "./Items";
import { nanoid } from "nanoid";

jest.mock("react-redux");
jest.mock("nanoid", () => {
  return { nanoid: () => "1234" };
});
jest.mock("nanoid");

describe("Items", () => {
  it("shoud check props", () => {
    const mockCallBack = jest.fn();
    const mockCallBackCounter = jest.fn();
    useSelector.mockReturnValue([]);
    const component = render(
      <Items
        favouritId={23}
        counter={mockCallBackCounter}
        addOrder={mockCallBack}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
