import React from "react";
import "../../scss/modal.scss";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import Button from "../button/Button";

export default function Modal({ buyItem, deleteOrder, closeModal, addOrder }) {
  const modal = useSelector((state) => state.modal);
  const modalHeader = modal.header;
  const modalText = modal.text;
  const modalId = modal.modal;
  const modalTextButton = modal.textButton;
  return (
    <>
      <div className="modal-content">
        <header>
          <h1>{modalHeader}</h1>
        </header>
        <span
          role={"close"}
          className="close"
          id="closeId"
          onClick={closeModal}
        >
          &times;
        </span>
        <p>{modalText}</p>
        {modalHeader === "Видалення з кошика" && (
          <Button
            modalId={modalId}
            text={modalTextButton}
            closeModal={closeModal}
            Click={deleteOrder}
          ></Button>
        )}
        {addOrder && (
          <Button
            modalId={modalId}
            text={modalTextButton}
            closeModal={closeModal}
            Click={addOrder}
          ></Button>
        )}
        {modalHeader === "Купівля" && (
          <Button
            modalId={modalId}
            text={modalTextButton}
            closeModal={closeModal}
            Click={buyItem}
          ></Button>
        )}
      </div>
    </>
  );
}

Modal.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  // closeModal: PropTypes.func.isRequired,
  typeofItem: PropTypes.string,
  addOrder: PropTypes.func,
};
Modal.defaultProps = {
  title: "Кошик",
  text: "Підтвердити",
  typeofItem: "true",
};
