import { fireEvent, render, screen } from "@testing-library/react";
import * as reactRedux from "react-redux";
import { useSelector } from "react-redux";
import * as reduxHooks from "react-redux";
import React from "react";
import Modal from "./Modal";

jest.mock("react-redux");
// jest.mock("react-redux", () => ({
//   ...jest.requireActual("react-redux"),
//   useSelector: jest.fn(),
// }));
jest.spyOn(reduxHooks, "useSelector").mockReturnValue({});

jest.mock("./Modal.js", () => ({
  modalHeader: jest.fn().mockReturnValue("myTestName"),
  modalText: jest.fn().mockReturnValue("myTest"),
  modalId: jest.fn().mockReturnValue(true),
  modalTextButton: jest.fn().mockReturnValue("myTestBuuton"),
}));
describe("modal shows the children and a close button", () => {
  //   jest.spyOn(reduxHooks, "useSelector").mockReturnValue({});
  //   const useSelectorMock = jest
  //     .spyOn(reactRedux, "useSelector")
  //     .mockReturnValue({});
  //   useSelectorMock.mockReturnValue({});
  //   beforeEach(() => {
  //     useSelectorMock.mockClear();
  //   });
  test("test functional madal", () => {
    const handleClose = jest.fn();
    const handleAdd = jest.fn();
    const handleDelete = jest.fn();
    const handleBuy = jest.fn();

    render(
      <Modal
        buyItem={handleBuy}
        addOrder={handleAdd}
        deleteOrder={handleDelete}
        closeModal={handleClose}
      />
    );
    // expect(getByText("test")).toBeTruthy();
    fireEvent.click(screen.getByRole("close"));
    expect(handleClose).toHaveBeenCalledTimes(1);
  });
});
