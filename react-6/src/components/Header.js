import React from "react";
import "../scss/heder.scss";
import { FaShoppingCart } from "react-icons/fa";
import { AiOutlineStar } from "react-icons/ai";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function Header({ orders, favouritId }) {
  const lenghtfavouritId = favouritId.length;
  const lenghtorders = orders.length;

  return (
    <div>
      <header className="header">
        <Link to="/selected">
          <AiOutlineStar className="aiOutlineStarHeader" />
        </Link>
        <Link className="Link Link__heder" to="/">
          Home
        </Link>
        <p className="counter">{favouritId ? lenghtfavouritId : 0}</p>
        <p className="item__number">{lenghtorders ? lenghtorders : 0}</p>
        <Link to={"/basket"}>
          {" "}
          <FaShoppingCart className="FaShoppingCart" />
        </Link>
      </header>
    </div>
  );
}
Header.propTypes = {
  favouritId: PropTypes.array.isRequired,
};
