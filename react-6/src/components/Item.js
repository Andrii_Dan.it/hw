import React from "react";
import "../scss/item.scss";
import "../scss/table_item.scss";
import { AiOutlineStar } from "react-icons/ai";
import { useState, useContext, useEffect } from "react";
import Modal from "./modal/Modal";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";
import { addOrderReduser, closeModalReduser } from "../store/modalReduser";
import { ThemeContext } from "../context/themeContext";

export default function Item({
  favouritId,
  addOrder,
  counter,
  name,
  url,
  price,
  id,
}) {
  const [star, setstar] = useState(false);
  const modal = useSelector((state) => state.modal);
  const modalBoolean = modal.modal;
  const dispatch = useDispatch();
  const { theme } = useContext(ThemeContext);

  useEffect(() => {
    if (favouritId.includes(id)) {
      setstar(true);
    } else {
      setstar(false);
    }
  }, []);
  const starCount = () => {
    if (!favouritId.includes(id)) {
      setstar(true);
    } else {
      setstar(false);
    }
    counter(id);
  };
  const openEnter = () => {
    return dispatch(addOrderReduser(id));
  };
  const closeModal = () => {
    return dispatch(closeModalReduser());
  };

  return (
    <div>
      {theme ? (
        <div id="container">
          <div className="card">
            <img className="card__photo" src={url} alt="Denim Jeans"></img>

            <div className="card__details">
              <span className="tag">{price} UAN</span>
              <div className="name">{name}</div>
              <AiOutlineStar
                style={{ color: star ? "red" : "" }}
                onClick={starCount}
                className="aiOutlineStarItem"
              />
              <button className="buttonCard" onClick={openEnter}>
                Add to card
              </button>
              {modalBoolean == id && (
                <Modal closeModal={closeModal} addOrder={addOrder}></Modal>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="container">
          <div className="table">
            <img
              className="margin__left table__photo"
              src={url}
              alt="Denim Jeans"
            ></img>

            <div className="table__details">
              <span className="margin__left table__tag">{price} UAN</span>
              <div className="margin__left table__name">{name}</div>
              <AiOutlineStar
                style={{ color: star ? "red" : "" }}
                onClick={starCount}
                className="margin__left aiOutlineStarItem"
              />
              <button
                className="margin__left table__buttonCard"
                onClick={openEnter}
              >
                Add to card
              </button>
              {modalBoolean == id && (
                <Modal closeModal={closeModal} addOrder={addOrder}></Modal>
              )}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

Item.propTypes = {
  addOrder: PropTypes.func.isRequired,
  counter: PropTypes.func.isRequired,
  color: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  typeofItem: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
};
