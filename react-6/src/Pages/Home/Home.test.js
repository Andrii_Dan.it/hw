import { render } from "@testing-library/react";
import Home from "./Home";

describe("Render Home", () => {
  it("should render Home", () => {
    const mockCallBack = jest.fn();
    const mockCallBackCont = jest.fn();
    const component = render(
      <Home
        orders={12}
        addOrder={mockCallBack}
        counter={mockCallBackCont}
        favouritId={23}
      />
    );
    expect(component).toMatchSnapshot();
  });
});
