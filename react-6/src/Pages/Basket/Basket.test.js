import { render } from "@testing-library/react";
import { BrowserRouter, Router } from "react-router-dom";
import Basket from "./Basket";
// import { createMemoryHistory } from "history";

jest.mock("react-redux");

describe("render basket", () => {
  //   const history = createMemoryHistory();
  it("should render", () => {
    const mockCallBack = jest.fn();
    const mockCallBackChange = jest.fn();
    const compare = render(
      <Basket
        deleteOrder={mockCallBack}
        order={[]}
        favouritId={[12, 34]}
        changeCount={mockCallBackChange}
      />,
      { wrapper: BrowserRouter }
    );

    expect(compare).toMatchSnapshot();
  });
});
